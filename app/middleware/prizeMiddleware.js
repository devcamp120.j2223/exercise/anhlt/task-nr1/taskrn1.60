const { request, response } = require("express");

const prizeMiddleware = (request, response, next)=>{
    console.log(`method: ${request.method}, url: ${request.url}`);
    next();
}
module.exports = prizeMiddleware;