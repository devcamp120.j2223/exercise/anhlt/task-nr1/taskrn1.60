// khai báo mongoose
const mongoose = require('mongoose');
// khai báo hàm Schema từ thư viện mongose
const Schema = mongoose.Schema;
// tạo Schema với các thuộc tính prize
const prizeSchema = Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('prize', prizeSchema);
